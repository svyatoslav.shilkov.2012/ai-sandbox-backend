import com.google.inject.AbstractModule
import services.{NNService, PerceptronService}


class Module extends AbstractModule {

  override def configure() = {
    bind(classOf[NNService]).to(classOf[PerceptronService])
  }

}
