package controllers.sockets

import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.stream.scaladsl._
import javax.inject.Inject
import play.api.libs.streams.ActorFlow
import play.api.mvc.{AbstractController, ControllerComponents, WebSocket}

import scala.concurrent.Future

class SocketController @Inject() (cc: ControllerComponents)(implicit system: ActorSystem, mat: Materializer) extends AbstractController(cc) {

  def socket: WebSocket = WebSocket.accept[String, String] { request =>
    // Log events to the console
    val in = Sink.foreach[String](println)

    // Send a single 'Hello!' message and then leave the socket open
    val out = Source.single("Hello!").concat(Source.maybe)

    Flow.fromSinkAndSource(in, out)
//    Future.successful {
//      Right(ActorFlow.actorRef { out =>
//        MyWebSocketActor.props(out)
//      })
//    }
  }

}
