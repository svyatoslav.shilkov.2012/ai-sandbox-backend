package controllers

import akka.actor.ActorSystem
import javax.inject._
import models.ImageData
import play.api.mvc._
import services.NNService

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class Controller @Inject()(cc: ControllerComponents,
                           actorSystem: ActorSystem,
                           percService: NNService)(implicit exec: ExecutionContext)
  extends AbstractController(cc) {

  def classify: Action[AnyContent] = Action.async { request =>
    Future {
      request.body.asJson.map(_.validate[ImageData]) match {
        case Some(js) => js.fold(
          _ => BadRequest("wrong json"),
          imData => Ok(percService.classify(imData).toJson)
        )

        case _ => BadRequest("no json")
      }
    }
  }

  def exampleNet: Action[AnyContent] = Action.async{ percService.getExample.map(n => Ok(n.toJson))}

}
