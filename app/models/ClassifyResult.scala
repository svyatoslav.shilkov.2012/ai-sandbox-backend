package models

import play.api.libs.json.{JsValue, Json, OFormat}

case class ClassifyResult(digits: Seq[(Int, Double)]){
  def toJson: JsValue = Json.toJson(this)
}

object ClassifyResult{
  implicit val format: OFormat[ClassifyResult] = Json.format[ClassifyResult]
}
