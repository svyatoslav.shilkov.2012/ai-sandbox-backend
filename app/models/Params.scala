package models

import libs.perceptron.PerceptronParams
import libs.perceptron.helpers.Implicits._
import play.api.libs.json.{Json, OFormat}

case class Params(weights: Array[Array[Array[Double]]], biases: Array[Array[Double]])

object Params{
  implicit val oFormat: OFormat[Params] = Json.format[Params]

  def fromPerceptronParams(p: PerceptronParams): Params = Params(
    weights = p.weights.map(_.to2DArray),
    biases = p.biases.map(_.toArray)
  )
}