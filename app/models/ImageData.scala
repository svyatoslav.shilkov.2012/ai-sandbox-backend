package models

import breeze.linalg.DenseVector
import play.api.libs.json.{Json, OFormat}

case class ImageData(data: Seq[Seq[Double]]){
  def input: DenseVector[Double] = DenseVector(data.flatten:_*)

}

object ImageData{
  implicit val format: OFormat[ImageData] = Json.format[ImageData]
}