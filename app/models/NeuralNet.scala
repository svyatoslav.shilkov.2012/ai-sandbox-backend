package models

import play.api.libs.json.{JsValue, Json, OFormat}

case class NeuralNet(structure: Seq[Int],
                     activationFunc: String,
                     withSoftMax: Boolean,
                     learningRate: Double,
                     epochs: Int,
                     batchSize: Int,
                     params: Params){
  def toJson: JsValue = Json.toJson(this)
}

object NeuralNet{
  implicit val oFormat: OFormat[NeuralNet] = Json.format[NeuralNet]
}
