package libs.matrices.vectors

import libs.matrices.Matrix

import scala.util.{Random, Success}

case class VectorsOfDifferentSizeException(size1: Int, size2: Int) extends RuntimeException(s"Diff size: $size1 : $size2")

case class MathVector(values: Vector[Double]) {

  import MathVector._

  def V: MathVector = this

  def asRowM: Matrix = Matrix(Vector(values))

  def asColM: Matrix = asRowM.T

  def replace(index: Int, value: Double): Vector[Double] = values.patch(index, Seq(value), 1)

  def +(that: MathVector): Vector[Double] = plus(that)

  def plus(that: MathVector): Vector[Double] = {
    checkSize(that)
    values.zip(that).map { case (a, b) => a + b }
  }


  def -(that: MathVector): Vector[Double] = minus(that)

  def minus(that: MathVector): Vector[Double] = plus(that * -1)

  private def normalizeInd(ind: Int) =
    if (ind % values.size == 0)
      ind
    else if (ind > 0)
      ind % values.size else values.size - ind.abs % values.size

  def get(ind: Int): Double = values(normalizeInd(ind))

  def pySlice(start: Int, end: Int = values.length): Vector[Double] = values.slice(normalizeInd(start), normalizeInd(end))

  def *(number: Double): Vector[Double] = multiply(number)

  def multiply(number: Double): Vector[Double] = values.map(number * _)

  def crossMultiply(that: MathVector): Vector[Vector[Double]] = mapEach(that, _ * _)

  def mapEach(that: MathVector, f: (Double, Double) => Double): Vector[Vector[Double]] =
    values.map(x => that.values.map(f(_, x)))


  def *(that: MathVector): Double = multiply(that)

  def multiply(that: MathVector): Double = {
    checkSize(that)
    this.zip(that).map { case (a, b) => a * b }.sum
  }

  def hadamardProd(that: MathVector): Vector[Double] = values.zip(that.values).map { case (x, y) => x * y }

  override def hashCode(): Int = values.hashCode()

  override def equals(that: Any): Boolean = that match {
    case vect: MathVector => canEqual(that) && values == vect.values
    case seq: Seq[Any] => canEqual(that) && values == seq
    case _ => false
  }

  override def canEqual(that: Any): Boolean = that.isInstanceOf[MathVector] ||
    that.isInstanceOf[Seq[Any]] && (
      that.asInstanceOf[Seq[Any]].forall(_.isInstanceOf[Double]) ||
        that.asInstanceOf[Seq[Any]].forall(_.isInstanceOf[Int])
      )

  private def checkSize(that: MathVector): Unit =
    if (this.size != that.size)
      throw VectorsOfDifferentSizeException(this.size, that.size)

  override def toString: String = values.mkString(" ")
}

object MathVector {
  implicit def fromSeqInt(seq: Seq[Int]): MathVector = MathVector(seq.toVector.map(_.toDouble))

  implicit def fromSeq(seq: Seq[Double]): MathVector = MathVector(seq.toVector)

  implicit def fromVector(vector: Vector[Double]): MathVector = MathVector(vector)

  //  implicit def fromVectorI(vector: Vector[Int]): MathVector = MathVector(vector.map(_.toDouble))

  implicit def toVector(mathVector: MathVector): Vector[Double] = mathVector.values

  def apply(values: Double*): MathVector = new MathVector(values.toVector)

  def random(len: Int): Vector[Double] = Vector.fill(len)(Random.nextDouble)

}
