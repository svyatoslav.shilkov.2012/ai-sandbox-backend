package libs.matrices

case class StrMatrix(stringMatrix: String) {
  def matrix: Matrix = Matrix.fromString(stringMatrix)
}

object StrMatrix{
  implicit def stringToStrMatrix(str: String): StrMatrix = StrMatrix(str)
  implicit def strMatrixToString(strMatrix: StrMatrix): String = strMatrix.stringMatrix
}