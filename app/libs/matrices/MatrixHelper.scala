package libs.matrices

import libs.matrices.Matrix._

import scala.collection.mutable.ArrayBuffer

object MatrixHelper {

  def stringToMatrix(string: String): V2 = string.trim.split("\n").toVector
      .map(_.split("\\s+").toVector
        .map(num => if (!num.last.isDigit) num.dropRight(1) else num)
        .map(_.toDouble)
      )

  def fillToRectangleMatrix(matrix: V2): V2 = {
    val rowLengths = matrix.map(_.length).distinct
    if (rowLengths.length == 1)
      return matrix
    val maxLen = (rowLengths :+ 0).max
    matrix.map{row => row ++ Vector.fill(maxLen - row.length)(0)}.asInstanceOf[V2]
  }

  def emptyBuffer(height: Int, width: Int): ArrayBuffer[ArrayBuffer[Double]] =
    ArrayBuffer.fill(height)(ArrayBuffer.fill(width)(0.0))

  def bufToVec(buf: ArrayBuffer[ArrayBuffer[Double]]): V2 = buf.map(_.toVector).toVector

}
