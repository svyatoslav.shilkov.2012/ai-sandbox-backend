package libs.matrices

import libs.matrices.vectors.MathVector

case class NotMatrixException(message: String = "Not matrix. Matrix rows are different length") extends RuntimeException(message)

case class DifferentSizeMatricesException(message: String = "Matrices are not compatible for this operation") extends RuntimeException(message)

/**
  * TODO:
  * Basic operations:
  * #  ######RESEARCH different algorithms and ways to calculate. Choose best
  * $   Addition $$$
  * $   Scalar multiplication $$$
  * $   Transposition $$$
  * Matrix multiplication
  * Row operations:
  * row addition
  * row multiplication
  * row switching
  * Submatrix
  * delete rows
  * delete columns
  * select rows
  * select columns
  *
  * # Linear transformations
  * #     Research####
  *
  * Square matrix
  * MainTypes
  * isTriangular
  * isUpperTriangular
  * isLowerTriangular
  * isDiagonal
  * isIdentity
  * isSymmetric
  * isScewSymmetric
  * isInvertible
  * #       ##RESEARCH
  * #      isDefinite
  * #        ###RESEARCH
  * isOrtogonal
  *
  * Main Operations
  * Trace
  * Determinant
  * #      Eigenvalues, eigenvectors
  * ###Research
  *
  * Matrix Decomposition
  * #      ###Research
  *
  *
  * поворот
  * Trace
  *
  * @param rows
  */

case class Matrix(rows: Vector[Vector[Double]]) {

  import MathVector._
  import Matrix._


  def M: Matrix = this

  //base
  def isEmpty: Boolean = height == 0 && rows.map(_.length).max == 0

  def dim: (Int, Int) = (height, width)

  def height: Int = rows.length

  def width: Int = rows.head.length

  def getRow(index: Int): MathVector = rows(index)

  def getColumn(index: Int): MathVector = rows.map(_ (index))

  lazy val columns: Vector[Vector[Double]] = T

  def mapValues(f: Double => Double): Vector[Vector[Double]] = rows.map(_.map(f))


  //base сhecks
  lazy val isRect: Boolean = rows.map(_.length).distinct.length == 1

  def sameShape(that: Matrix): Boolean = dim == that.dim



  //base checks exception throwers
  private def checkRects(that: Matrix, operationName: String): Unit = {
    checkRect(s"Cannot $operationName. First matrix is not rectangle")
    that.checkRect(s"Cannot $operationName. Second matrix is not rectangle")
  }

  private def checkRect(message: String = ""): Unit = if (!isRect) {
    if (message.nonEmpty)
      throw NotMatrixException(message)
    else throw NotMatrixException()
  }

  private def checkShape(that: Matrix): Unit =
    if (!sameShape(that)) throw DifferentSizeMatricesException(s"Different matrixes. $dim : ${that.dim}")

  private def checkMultiply(that: Matrix): Unit = if (width != that.height)
      throw DifferentSizeMatricesException(s"Cannot multiply. $dim * ${that.dim}")

  //base operations
  def T: V2 = transpose

  def transpose: V2 = {
    checkRect()
    if (height == 0 || width == 0)
      return empty
    rows.transpose
  }


  def +(that: Matrix): V2 = plus(that)

  def plus(that: Matrix): V2 = {
    checkRects(that, "add")
    checkShape(that)
    rows.zip(that.rows).map { case (a, b) => a + b }
  }


  def -(that: Matrix): V2 = minus(that)

  def minus(that: Matrix): V2 = plus(that * -1)


  def *(number: Double): V2 = multiply(number)

  def multiply(number: Double): V2 = {
    checkRect("Cannot multiply on number. Matrix is not rectangle")
    rows.map(_ * number)
  }


  def *(that: Matrix): V2 = multiply(that)

  def multiply(that: Matrix): V2 = {
    checkRects(that, "multiply")
    checkMultiply(that)
    val columnsThat = that.columns
    rows.map(row => columnsThat.map(row * _))
  }


  def multiplyVec(that: MathVector): Vector[Double] = rows.map(_ * that)


  //base helpers
  def print: Unit = println(this)

  def fill(value: Double): V2 = Vector.fill(height)(Vector.fill(width)(value))

  def fillToMatrix: Matrix = Matrix.fillToMatrix(rows)

  def zipMap[B](that: Matrix, op: (Double, Double) => B): Vector[Vector[B]] =
    rows.zip(that.rows).map{case (a, b) => a.zip(b).map(op.tupled)}



  //base overridings
  override def hashCode(): Int = rows.hashCode()

  override def equals(that: Any): Boolean = that match {
    case that if canEqual(that) => rows == that
    case _ => false
  }

  override def canEqual(that: Any): Boolean = that.isInstanceOf[Matrix] || that.isInstanceOf[Seq[Seq[Double]]] || that.isInstanceOf[Seq[Seq[Int]]]

  override def toString: String = rows.map(_.map(valToStr(_)).mkString(" ")).mkString("\n")

  def toUglyString: String = rows.map(_.mkString(" ")).mkString("\n")

  private def valToStr(value: Double, len: Int = 5) = value.toString take len

}

object Matrix {

  import MathVector._

  type V2 = Vector[Vector[Double]]


  //implicit conversions
  implicit def seq2DoubleToMatrix(seq2: Seq[Seq[Double]]): Matrix = Matrix(seq2.map(_.toVector).toVector)

  implicit def seq2IntToMatrix(seq2: Seq[Seq[Int]]): Matrix = Matrix(seq2.map(_.toVector.map(_.toDouble)).toVector)

  implicit def seq2ByteToMatrix(seq2: Seq[Seq[Byte]]): Matrix = Matrix(seq2.map(_.toVector.map(_.toDouble)).toVector)

  implicit def mathVectorsToMatrix(vectors: Vector[MathVector]): Matrix = Matrix(vectors.map(_.toVector))

  implicit def fromVectorOfVectors(values: V2): Matrix = Matrix(values)

  implicit def toVectorOfVectors(matrix: Matrix): V2 = matrix.rows



  //constructors
  def apply(rows: Seq[Seq[Int]]): Matrix = Matrix.fillToMatrix(rows.toVector.map(_.toVector.map(_.toDouble)))

  def apply(single: Double): Matrix = new Matrix(Vector(Vector(single)))

  def apply(): Matrix = empty

  def empty: Matrix = Vector[Vector[Double]]()

  def empty(rows: Int, cols: Int): Matrix = Vector.fill(rows, cols)(0.0)

  def random(rows: Int, cols: Int): Vector[Vector[Double]] = Vector.fill(rows)(MathVector.random(cols))

  //conversions
  def fromString(str: String): V2 = MatrixHelper.stringToMatrix(str)

  def fillToMatrix(values: V2): V2 = MatrixHelper.fillToRectangleMatrix(values)


}