package libs.measure

object MeasureUtils{
  def measureAvgMillis(action: Unit => Unit, times: Int = 10): Long = measAvg(action, times, measureMillis)

  def measureMillis[R](action: Unit => R): (R, Long) = measureNano(action) match {
    case (r, nano) => r -> nano / 1000 / 1000
  }

  def measureAvgNano(action: Unit => Unit, times: Int = 10): Long = measAvg(action, times, measureNano)

  def measureNano[R](action: Unit => R): (R, Long) = {
    val s = System.nanoTime()
    val r = action()
    (r, System.nanoTime() - s)
  }

  private def measAvg(action: Unit => Unit, times: Int = 10, mF: (Unit => Unit) => (_, Long)): Long = {
    ((1 to times).map(_ => mF(action)._2).sum.toDouble / times).toLong
  }
}