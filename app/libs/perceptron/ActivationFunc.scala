package libs.perceptron

import libs.perceptron.helpers.Math._

trait ActivationFunc{
  val name: String
  val call: Fun
  val deriv: Fun = derivative(call)
}

object ActivationFunc {

  val ReLU0Derivative: Double = 0.5
  val ELUFactorA: Double = 0.2

  val Sigmoid: ActivationFunc = new ActivationFunc{
    override val name: String = "Sigmoid"
    override val call: Fun = (x: Double) => 1 / (1 + math.exp(-x))
    override val deriv: Fun = (x: Double) => call(x) * (1 - call(x))
  }

  val ReLU: ActivationFunc = new ActivationFunc{
    override val name: String = "ReLU"
    override val call: Fun = math.max(0, _)
    override val deriv: Fun = {
      case negative if negative < 0 => 0
      case 0 => 0.5
      case positive => 1
    }
  }

  val LeakyReLU: ActivationFunc = new ActivationFunc{
    override val name: String = "Leaky_ReLU"
    override val call: Fun = {
      case negative if negative < 0 => 0.01 * negative
      case zeroOrPositive => zeroOrPositive
    }
    override val deriv: Fun = {
      case negative if negative < 0 => 0
      case 0 => ReLU0Derivative
      case positive => 1
    }
  }

  val ELU: ActivationFunc = new ActivationFunc{
    override val name: String = "ELU"
    override val call: Fun = {
      case negative if negative < 0 => ELUFactorA * (math.exp(negative) - 1)
      case zeroOrPositive => zeroOrPositive
    }
    override val deriv: Fun = {
      case negative if negative < 0 => ELUFactorA * math.exp(negative)
      case 0 => ReLU0Derivative
      case positive => 1
    }
  }

  val functions: Seq[ActivationFunc] = Seq(Sigmoid, ReLU, LeakyReLU, ELU)


}
