package libs.perceptron.helpers

import breeze.linalg.{DenseMatrix, DenseVector}
import libs.perceptron.PerceptronParams.NetConnections

object Implicits {

  type DV = DenseVector[Double]
  type DM = DenseMatrix[Double]

  implicit class SmartVector(v: DV){
    def crossMultiply(that: DV): DM = {
      val r = v.map(x => that.map(y => x * y))
      DenseMatrix(r.toArray:_*)
    }
  }

  implicit class Connection(c: (Int, Int, Int, Double)){
    def layer: Int = c._1
    def neuronFrom: Int = c._2
    def neuronTo: Int = c._3
    def value: Double = c._4
  }

  implicit class SmartMatrix(m: DM){
    def to2DArray: Array[Array[Double]] = m.toArray.grouped(m.rows).toArray.transpose

    def toConnectionsList(layer: Int): NetConnections = to2DArray.zipWithIndex.flatMap{
      case (row, fromInd) => row.zipWithIndex.collect{case (el, toInd) if el != 0 => (layer, fromInd, toInd, el)}
    }

  }

    object SmartMatrix {
      def toDM(rows: Int, cols: Int, connections: NetConnections): DM = {
        val m = DenseMatrix.zeros[Double](rows, cols)
        connections.foreach{c => m(c.neuronFrom, c.neuronTo) = c.value}
        m
      }
    }

}
