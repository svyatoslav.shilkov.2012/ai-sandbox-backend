package libs.perceptron.helpers

import breeze.linalg.DenseVector

object Math {

  type Fun = Double => Double

  type V = Vector[Double]

  implicit def fToV(f: Fun): DenseVector[Double] => DenseVector[Double] = (x: DenseVector[Double]) => x.map(f)

  def derivative(fun: Fun): Fun = (x: Double) => (fun(x) - fun(x - 1e-14)) / 1e-14

}
