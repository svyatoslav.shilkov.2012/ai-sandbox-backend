package libs.perceptron.helpers

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

object AsyncTools {

  implicit val ex: ExecutionContext = ExecutionContext.global

  implicit class FutureSeq[T](seq: Seq[Future[T]]){
    def toFuture: Future[Seq[T]] = Future.sequence(seq)
    def getSeqResult: Seq[T] = getSeqResultAfter(10)
    def getSeqResultAfter(atMostSeq: Int): Seq[T] = toFuture.getAfter(atMostSeq)
  }

  implicit class SmartFuture[T](future: Future[T]) {

    def get: T = getAfter(5)

    def getAfter(atMostSeconds: Int): T = Await.result(future, atMostSeconds seconds)

  }

}
