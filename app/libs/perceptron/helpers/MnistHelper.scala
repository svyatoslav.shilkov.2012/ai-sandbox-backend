package libs.perceptron.helpers

import java.io.{BufferedInputStream, DataInputStream, File, FileInputStream}

import breeze.linalg.DenseVector
import libs.perceptron.helpers.Implicits.DV

object MnistHelper {

  private val TestImagesFile = new File("trainData/t10k-images-idx3-ubyte")
  private val TestLabelsFile = new File("trainData/t10k-labels-idx1-ubyte")
  private val TrainImagesFile = new File("trainData/train-images-idx3-ubyte")
  private val TrainLabelsFile = new File("trainData/train-labels-idx1-ubyte")

  type Image = Array[Array[Int]]

  lazy val testData: Array[(Int, Image)] = readLabelDataset(TestLabelsFile) zip readImageDataset(TestImagesFile)

  lazy val trainData: Array[(Int, Image)] = readLabelDataset(TrainLabelsFile) zip readImageDataset(TrainImagesFile)

  lazy val normalizedTrainData: Seq[(Int, DV)] = normalized(trainData)

  lazy val normalizedTestData: Seq[(Int, DV)] = normalized(testData)

  private def normalized(data: Array[(Int, Image)]): Seq[(Int, DV)] = data.map{case (l, i) => l -> DenseVector(i.flatten.map(_ / 255.0):_*)}

  private def readImageDataset(file: File): Array[Image] = {
    val inputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)))
    val magic = inputStream.readInt()
    val imagesCount: Int = inputStream.readInt()
    val rows: Int = inputStream.readInt()
    val cols: Int = inputStream.readInt()
    val images = Array.fill(imagesCount)(Array.fill(28, 28)(inputStream.readUnsignedByte()))
    inputStream.close()
    images
  }

  private def readLabelDataset(file: File): Array[Int] = {
    val inputStream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)))
    val magic = inputStream.readInt()
    val labelsCount: Int = inputStream.readInt()
    val labels = Array.fill(labelsCount)(inputStream.readUnsignedByte())
    labels
  }

}
