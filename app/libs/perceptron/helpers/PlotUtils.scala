package libs.perceptron.helpers

import Math._
import breeze.linalg.DenseMatrix
import breeze.plot.{Figure, image}
import libs.perceptron.helpers.Implicits.DV

object PlotUtils {

  val Height = 100

  def round(num: Double, signsAfterComa: Int): Double = {
    val div = math.pow(10, signsAfterComa)
    (num * div).toInt / div
  }

  def normalizeRight(num: Double, len: Int): String = normalizeRightStr(num.toString, len)

  def normalizeRightStr(str: String, len: Int): String =  " " * (len - str.length) + str.take(len)

  def plot(data: Seq[Double], limit: Int = 100): Unit = plot(data, (0 to data.length).map(_.toString), limit)

  def plot(data: Seq[Double], ticks: Seq[String], limit: Int): Unit = {
    println
    val used = data.zip(ticks).grouped(math.max(1, data.length / limit)).map(_.head).toVector
    val longestTick = used.map(_._2.length).max
    used.map{case (d, t) => normalizeRightStr(t, longestTick) + ":" + "*" * d.toInt} foreach println
    println
  }

  def plotFunc(f: Fun, step: Double = 0.1, s: Double = -4.0, e: Double = 4.0, maxYOpt: Option[Double] = None): Unit = {
    val vals = (0 until ((e - s)/step).toInt).map(i => s + step * i).map(x => x -> f(x))
    val maxY = maxYOpt.getOrElse(vals.map(_._2).max)
    val longestX = vals.map(_._1.toString.length).max
    vals.map{case (x, y) => normalizeRight(x, longestX) + ":" + "*" * (Height / maxY * y).toInt} foreach println
  }

  def measure[A, B](f: Unit => B): Long = {
    val s = System.currentTimeMillis()
    f()
    val millis = System.currentTimeMillis() - s
    millis
  }

  def plotImages(images: Seq[(Int, DV)]): Unit = {
    val f = Figure("im")
    f.cols = math.min(10, images.length)
    f.rows = math.max(1, images.length / 10 + 1)

    images.zipWithIndex.foreach {
      case ((label, imdata), i) =>
        val im = DenseMatrix(imdata.toArray.grouped(28).toArray.reverse: _*)
        f.subplot(i) += image(im)
        f.subplot(i).title = label + ""
        f.refresh()
    }
  }

}
