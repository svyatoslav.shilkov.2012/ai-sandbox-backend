package libs.perceptron

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import breeze.linalg._
import libs.perceptron.helpers.Implicits._
import play.api.libs.json.{Json, OFormat}

import scala.util.Random

@SerialVersionUID(100L)
case class PerceptronParams(weights: Array[DM],
                            biases: Array[DV]) extends Serializable {
  @transient lazy val inputSize: Int = weights.head.rows
  @transient lazy val outputSize: Int = weights.last.cols
  @transient lazy val neuronLayersCount: Int = weights.length + 1
  @transient lazy val shape: Seq[Int] = weights.map(_.rows) :+ weights.last.cols

  def weightsAndBiases: Array[(DM, DV)] = weights zip biases

  def +(that: PerceptronParams): PerceptronParams = PerceptronParams(
    weights.zip(that.weights).map { case (w1, w2) => w1 + w2 },
    biases.zip(that.biases).map { case (b1, b2) => b1 + b2 }
  )

  def -(that: PerceptronParams): PerceptronParams = PerceptronParams(
    weights.zip(that.weights).map { case (w1, w2) => w1 - w2 },
    biases.zip(that.biases).map { case (b1, b2) => b1 - b2 }
  )

  def / (num: Double): PerceptronParams = PerceptronParams(
    weights.map(_ /:/ num),
    biases.map(_  /:/ num)
  )

  def * (num: Double): PerceptronParams = PerceptronParams(
    weights.map(_.mapValues(_ * num)),
    biases.map(_ * num)
  )

  def prune(rate: Double): PerceptronParams = {
    val connections = PerceptronParams.toConnectionList(weights).sortBy(_.value.abs)(Ordering.Double.reverse)
    val keeped = connections.take((connections.length * (100.0 - rate) / 100.0).toInt)
    val newW = PerceptronParams.toDMs(this, keeped)
    PerceptronParams(newW, biases)
  }

  lazy val prunedW: Int = weights.map(_.findAll(_ == 0.0).length).sum

}

object PerceptronParams {

  def load(filePath: String): PerceptronParams = {
    val ois = new ObjectInputStream(new FileInputStream(filePath))
    val params = ois.readObject.asInstanceOf[PerceptronParams]
    ois.close
    params
  }

  def save(params: PerceptronParams, filePath: String): Unit = {
    val oos = new ObjectOutputStream(new FileOutputStream(filePath))
    oos.writeObject(params)
    oos.close
  }

  type NetConnections = Array[(Int, Int, Int, Double)]


  def toConnectionList(weights: Array[DM]): NetConnections = weights.zipWithIndex.flatMap{
    case (layerW, layerInd) => layerW.toConnectionsList(layerInd)
  }

  def toDMs(params: PerceptronParams, list: NetConnections): Array[DM] = params.weights.zipWithIndex.map{
    case (oldW, index) => SmartMatrix.toDM(oldW.rows, oldW.cols, list.filter(_.layer == index))
  }

  def apply(layerNeurons: Int*): PerceptronParams = filled(Random.nextGaussian() / math.sqrt(_), layerNeurons:_*)

  def zeros(layerNeurons: Int*): PerceptronParams = filled(_ => 0, layerNeurons:_*)

  def filled(neuronsToValue: Int => Double, layerNeurons: Int*): PerceptronParams = {
    val lifted = layerNeurons.lift
    val pairedWithNextLayer = layerNeurons
      .zipWithIndex
      .flatMap {
        case (neuronsCount, ind) => lifted(ind + 1).map(neuronsCount -> _)
      }
    val weights = pairedWithNextLayer.map {
      case (neurons, nextLayerNeurons) => DenseMatrix.fill(neurons, nextLayerNeurons)(neuronsToValue(neurons))
    }.toArray
    val biases = pairedWithNextLayer.map {
      case (neurons, nextLayerNeurons) => DenseVector.fill(nextLayerNeurons)(neuronsToValue(neurons))
    }.toArray

    PerceptronParams(weights, biases)
  }



  def sum(list: Seq[PerceptronParams]): PerceptronParams = list.reduceLeft(_ + _)

  def avg(list: Seq[PerceptronParams]): PerceptronParams = {
    val s = sum(list)
    PerceptronParams(
      s.weights.map(_.mapValues(_ / list.length)),
      s.biases.map(_.map(_ / list.length))
    )
  }

}