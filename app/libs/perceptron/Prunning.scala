package libs.perceptron

import breeze.linalg.{DenseMatrix, DenseVector, inv}
import breeze.plot._
import libs.measure.MeasureUtils._
import libs.perceptron.helpers.Implicits._
import libs.perceptron.helpers.{MnistHelper, PlotUtils}

import scala.collection.immutable

import Usefull._

object Prunning extends App {
  val Trained_b5_e30 = "trained/acc_96.21-lr_0.1-batch_5-epoch_30-784x30x10.net"
  val Trained_b1_e30 = "trained/acc_95.85-lr_0.01-batch_1-epoch_30-784x30x10.net"
  val Trained_b5_e30_2 = "trained/acc_96.92-lr_0.01-batch_5-epoch_30-784x30x10.net"
  val Trained_b50_e3 = "trained/acc_91.12-lr_0.01-batch_50-epoch_3-784x30x10.net"
  val Trained_b2000_e30 = "trained/acc_91.3-lr_0.1-batch_2000-epoch_30.net"
//  val p = load(Trained_b1_e30)

//  brainDamage
  dummyPrunningTest

  def brainDamage: Unit = {
        val p = process(30, 500, 0.01, immutable.Vector(28 * 28, 30, 10), func = ActivationFunc.LeakyReLU,
          testDuringTrain = true, testEach = 1000, plot = true, 100)
  }

  def dummyPrunningTest = {
    val p = load(Trained_b2000_e30)
    val res = (1 to 100).map(_.toDouble).map(r => r -> p.pruned(r).test(MnistHelper.normalizedTestData))
    res foreach println
    val (rate, acc) = res.unzip

    val f = Figure("Prunning")
    val pl = f.subplot(0)
    pl.xlabel = "prunning rate"
    pl.ylabel = "accuracy"
    pl += plot(rate, acc)
  }


}

object Usefull{
  def load(file: String): Perceptron = Perceptron.load(file, ActivationFunc.ReLU)


  def process(ep: Int, batch: Int, lr: Double,
              arch: immutable.Vector[Int] = immutable.Vector(28 * 28, 30, 10),
              func: ActivationFunc = ActivationFunc.ReLU,
              testDuringTrain: Boolean = false,
              testEach: Int = 10,
              plot: Boolean = false,
              plotFPS: Int = 10): Perceptron = {
    print("reading data...")
    MnistHelper.normalizedTrainData
    MnistHelper.normalizedTestData
    print("\rgenerating...")
    val digits = Perceptron.generate(func, arch: _*)
    print("\rgenerated! training...")
    val (_, time) = measureMillis(_ => digits.SGD(MnistHelper.normalizedTrainData, batch, ep, lr,
      testDuringTrain, testEach, plot, plotFPS))
    print("\rtrained! testing...")
    val acc = digits.test(MnistHelper.normalizedTestData)
    digits.save(acc, lr, batch, ep)
    println(s"\rep=$ep batch=$batch lr=$lr acc=$acc  $time")
    digits
  }

  def testAndPlotWrong(p: Perceptron): Unit = {
    val wrong = p.testReturnWrong(MnistHelper.normalizedTestData)
    wrong.groupBy(_._1).values.foreach(PlotUtils.plotImages)
  }

}
