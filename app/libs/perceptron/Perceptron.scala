package libs.perceptron

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import libs.perceptron.helpers.AsyncTools._
import libs.perceptron.helpers.Math._
import libs.perceptron.helpers.PlotUtils._
import ActivationFunc._
import breeze.linalg._
import breeze.plot.{Figure, plot, image}
import libs.perceptron.helpers.{MnistHelper, PlotUtils}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Random, Try}
import helpers.Implicits._


class Perceptron(var params: PerceptronParams,
                 activationFunc: ActivationFunc) {

  implicit val ex: ExecutionContext = ExecutionContext.global
  val ParallelFactor: Int = Runtime.getRuntime.availableProcessors()

  def countPruned: Int = params.prunedW

  def dummyPruning(rate: Double): Unit = params = params.prune(rate)

  def pruned(rate: Double): Perceptron = {
    val c = copy
    c.dummyPruning(rate)
    c
  }

  def processSample(labelAndInput: (Int, DV)): (Boolean, Double, PerceptronParams) = {
    val (label, inputData) = labelAndInput
    val (ans, activations, zs) = forward(inputData)
    val delta = backpropagation(activations, zs, targetVector(label))
    (ans == label, costFunction(activations.last, label), delta)
  }

  def processGroup(inputs: Seq[(Int, DV)]): (Int, Double, PerceptronParams) =
    inputs.foldLeft((0: Int, 0.0: Double, PerceptronParams.zeros(params.shape: _*))) {
      case ((guessedCnt, lossSum, curSum), (label, input)) =>
        val (isCorrect, loss, deltas) = processSample((label, input))
        (guessedCnt + (if (isCorrect) 1 else 0), lossSum + loss, curSum + deltas)
    }

  def processBatchParallel(batch: Seq[(Int, DV)]): (Int, Double, PerceptronParams) = {
    val (correctCnt, totalLossSum, deltasSum) = batch.grouped(math.max(1, batch.length / ParallelFactor))
      .map(g => Future(processGroup(g)))
      .toVector.getSeqResultAfter(3000)
      .foldLeft((0: Int, 0.0: Double, PerceptronParams.zeros(params.shape: _*))) {
        case ((guessedCnt, lossSum, totalSum), (curCnt, curLossSum, curSum)) =>
          (guessedCnt + curCnt, lossSum + curLossSum, totalSum + curSum)
      }

    (correctCnt, totalLossSum / batch.length, deltasSum / batch.length)
  }


  def SGD(trainData: Seq[(Int, DV)], batchSize: Int, numEpochs: Int, learningRate: Double,
          testEachStep: Boolean = false, testEach: Int = 10, plotProgress: Boolean = false, plotFPS: Int = 10): Unit = {
    val allAcc = ArrayBuffer[(Double, Double)]()
    val allLoss = ArrayBuffer[(Double, Double)]()
    val testAcc = ArrayBuffer[(Double, Double)]()

    val epochSteps = trainData.length / batchSize
    val totalSteps = numEpochs * epochSteps
    for (e <- 1 to numEpochs) {
      val data = Random.shuffle(trainData)
      val correct = data.grouped(batchSize).zipWithIndex.foldLeft(0 -> 0) { case ((total, right), (batch, i)) =>
        val (correctCnt, avgLoss, delta) = processBatchParallel(batch)
        params += delta * learningRate
        val newTotal = total + batchSize
        val newRight = right + correctCnt
        val batchAcc = round(correctCnt * 100.0 / batchSize, 2)
        val step = curStep(e, epochSteps, i)
        allAcc.append((step, batchAcc))
        allLoss.append((step, avgLoss * 100.0))
        val curTestAcc = if(testEachStep && step % testEach == 0)
          Some(test(MnistHelper.normalizedTestData)) else None
        curTestAcc.foreach(cta => testAcc.append((step, cta)))

        if(plotProgress)
          plotAcc(allAcc.toArray, allLoss.toArray, testAcc.toArray, plotCurAcc=testEachStep, fps=plotFPS)

        printStepInfo(e, i, numEpochs, epochSteps, totalSteps, batchAcc, avgLoss, newTotal, newRight, curTestAcc)
        (newTotal, newRight)
      }
    }

  }

  var f: Option[Figure] = None

  private def plotAcc(acc: Array[(Double, Double)],
                      loss: Array[(Double, Double)],
                      testAcc: Array[(Double, Double)],
                      plotCurAcc: Boolean = false,
                      fps: Int = 10) = {
    if(f.isEmpty)
      f = Some(Figure())
    if(acc.length % fps == 0) {
      f.foreach { ff =>
        val (rng, accV) = acc.unzip
        val lossV = loss.map(_._2)
        val (tArng, tAccV) = testAcc.unzip
        ff.clear()
        val p = ff.subplot(0)
        p += plot(DenseVector(rng: _*), DenseVector(accV: _*))
        p += plot(DenseVector(rng: _*), DenseVector(lossV: _*))
        if (plotCurAcc)
          p += plot(DenseVector(tArng: _*), DenseVector(tAccV: _*), style = '.', colorcode = "red")
        ff.refresh()
      }
    }
  }

  var lastStepsTime: Array[Long] = Array()

  def calcTime(stepsLeft: Int): String = {
    val curTime = System.nanoTime()
    val avg = (curTime - lastStepsTime.headOption.getOrElse(0L)) / Math.max(lastStepsTime.length, 1)
    lastStepsTime = lastStepsTime :+ curTime
    if (lastStepsTime.length > 1000)
      lastStepsTime = lastStepsTime.drop(1)
    val totalSecsLeft = avg * stepsLeft / 1000 / 1000 / 1000
    val hours = totalSecsLeft / 3600
    val minutes = totalSecsLeft % 3600 / 60
    val seconds = totalSecsLeft % 60
    s"$hours h. $minutes m. $seconds s."
  }

  private def curStep(epochI: Int, epochSteps: Int, batchI: Int): Int = (epochI - 1) * epochSteps + batchI + 1

  def printStepInfo(epochI: Int, batchI: Int,
                    numEpochs: Int, epochSteps: Int,
                    totalSteps: Int, batchAcc: Double, batchLoss: Double,
                    newTotal: Int, newRight: Int, testAcc: Option[Double]) = {

    val totalAcc: Double = newRight * 100.0 / newTotal

    val curSteps = curStep(epochI, epochSteps, batchI)
    val timeLeft = calcTime(totalSteps - curSteps)

    val progress = curSteps * 100.0 / totalSteps
    val progressBar = normalizeRight(progress, 5) +
      "% (" + "|" * progress.toInt + "-" * (100 - progress.toInt) +
      s") B_ACC=${round(batchAcc, 2)}% B_LOSS=${round(batchLoss * 100, 2)}% " +
      s"TOTAL_ACC=${round(totalAcc, 2)}% " +
      s"${testAcc.map(a => s"TEST_ACC=${round(a, 2)}%").getOrElse("")}    TIME_LEFT: $timeLeft"
    print("\r" + progressBar)
  }


  def test(data: Seq[(Int, DV)]): Double = {
    data.count { case (label, im) => forward(im)._1 == label } * 100.0 / data.length
  }

  def testReturnWrong(data: Seq[(Int, DV)]): Seq[(Int, DV)] = data.filter{case (label, im) => forward(im)._1 != label }

  def classify(input: DV): Array[Double] = forward(input)._2.last.toArray

  def forward(input: DV): (Int, Array[DV], Array[DV]) = {
    require(input.length == params.inputSize, "Input size must match first layer size")
    val (activations, zs) = params.weightsAndBiases.foldLeft(Array(input), Array[DV]()) {
      case ((prevActivations, calculatedZs), (weights, biases)) =>
        val layerInput = prevActivations.last
        val z = (weights.t * layerInput) + biases
        val activation = activationFunc.call(z)
        (prevActivations :+ activation, calculatedZs :+ z)
    }
    (argmax(activations.last), activations.dropRight(1) :+ Perceptron.softMax(activations.last), zs)
  }

  def backpropagation(activations: Array[DV], zs: Array[DV], target: DV): PerceptronParams = {

    val costDerivative = target - activations.last
    val lastBiasDelta = costDerivative *:* activationFunc.deriv(zs.last)
    val lastWeightDelta = (lastBiasDelta crossMultiply activations.dropRight(1).last).t

    val activationsZsWeights = activations.dropRight(2) zip zs.dropRight(1) zip params.weights.drop(1).zipWithIndex


    val (deltasW, deltasB) =
      activationsZsWeights.foldRight((Array[DM](lastWeightDelta), Array(lastBiasDelta))) {
        case (((neurons, nextZs), (weights, i)), (weightsDeltas, biasesDeltas)) =>
          //            println(s"step=$i")
          val activationsDeltas: DV = weights * biasesDeltas.head *:* activationFunc.deriv(nextZs)
          //slow part. fire together - wire together
          val curWeightsDeltas: DM = (activationsDeltas crossMultiply neurons).t

          (curWeightsDeltas +: weightsDeltas, activationsDeltas +: biasesDeltas)
      }

    PerceptronParams(deltasW, deltasB)
  }

  def save(acc: Double, lr: Double, batchSize: Int, numEpochs: Int) = {
    val accStr = acc.toString.take(5)
    val lrStr = lr.toString.take(5)
    val batchStr = batchSize.toString
    val numEStr = numEpochs.toString

    val filePath = s"trained/acc_$accStr-lr_$lrStr-batch_$batchStr-epoch_$numEStr-${params.shape.mkString("x")}.net"
    PerceptronParams.save(params, filePath)
  }

  def costFunction(output: DV, label: Int): Double =
    sum((output - targetVector(label)).map(math.pow(_, 2)))

  def targetVector(label: Int): DV = {
    val v = DenseVector.zeros[Double](params.outputSize)
    v(label) = 1.0
    v
  }

  def copy: Perceptron = new Perceptron(params, activationFunc)

}

object Perceptron {

  def zeros(activation: ActivationFunc, layers: Int*): Perceptron = new Perceptron(PerceptronParams.zeros(layers: _*), activation)

  def zeros(layers: Int*): Perceptron = new Perceptron(PerceptronParams.zeros(layers: _*), ReLU)

  def generate(layers: Int*): Perceptron = generate(ReLU, layers: _*)

  def generate(activation: ActivationFunc, layers: Int*): Perceptron = new Perceptron(PerceptronParams(layers: _*), activation)

  def softMax(logits: DV): DV = {
    //    softmax(logits)
    val exps = logits.map(math.exp)
    val sm = sum(exps)
    exps.map(_ / sm)
  }

  def load(file: String, activationFunc: ActivationFunc): Perceptron = {
    new Perceptron(PerceptronParams.load(file), activationFunc)
  }

}
