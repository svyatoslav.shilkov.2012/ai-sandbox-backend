package libs.perceptron

import libs.matrices.Matrix._
import libs.perceptron.helpers.Math._
import libs.perceptron.ActivationFunc._
import libs.perceptron.helpers.{MnistHelper, PlotUtils}

import scala.util.{Random, Success, Try}

object XorDataSet {
  private val AllCases = Vector(
    0 -> Vector(1.0, 1),
    1 -> Vector(1.0, 0),
    1 -> Vector(0.0, 1),
    0 -> Vector(0.0, 0)
  )

  private lazy val allGenerated = Random.shuffle(Vector.fill(15)(AllCases).flatten)

  lazy val train: Vector[(Int, Vector[Double])] = Random.shuffle(Vector.fill(10)(AllCases).flatten)
  lazy val test: Vector[(Int, Vector[Double])] = AllCases
}

import libs.measure.MeasureUtils._

object Main extends App {
  digits

  def digits = {
    print("reading data...")
    MnistHelper.normalizedTrainData
    MnistHelper.normalizedTestData

    def process(ep: Int, batch: Int, lr: Double, arch: Vector[Int] = Vector(28 * 28, 30, 10)): Unit = {
      print("\rgenerating...")
      val digits = Perceptron.generate(arch:_*)
      print("\rgenerated! training...")
      val (_, time) = measureMillis(_ => digits.SGD(MnistHelper.normalizedTrainData, batch, ep, lr))
      print("\rtrained! testing...")
      val acc = digits.test(MnistHelper.normalizedTestData)
      digits.save(acc, lr, batch, ep)
      println(s"\rep=$ep batch=$batch lr=$lr acc=$acc  $time")
    }

    Vector(
      (100, 1, 0.001, Vector(28 * 28, 30, 10)),
      (100, 1, 0.01, Vector(28 * 28, 30, 10)),
      (100, 5, 0.1, Vector(28 * 28, 30, 10)),
      (100, 5, 0.01, Vector(28 * 28, 30, 10)),
      (100, 5, 0.001, Vector(28 * 28, 30, 10)),
      (1000, 60000, 1.0, Vector(28 * 28, 30, 10)),

      (100, 5, 0.01, Vector(28 * 28, 16, 16, 10)),
      (100, 5, 0.1, Vector(28 * 28, 16, 16, 10)),
      (1000, 60000, 1.0, Vector(28 * 28, 16, 16, 10)),
    ).foreach{case (e, b, l, a) => process(e, b, l, a)}

    Vector(100).foreach { ep =>
      Vector(1, 5, 10, 2000, 5000, 60000).foreach { batch =>
        Vector(0.1, 0.01, 0.001).foreach { lr =>

        }
      }
    }
  }

}
