package services

import javax.inject.Inject
import libs.perceptron.{ActivationFunc, Perceptron, PerceptronParams}
import models.{ClassifyResult, ImageData, NeuralNet, Params}

import scala.concurrent.{ExecutionContext, Future}

class PerceptronService @Inject()(implicit ex: ExecutionContext) extends NNService {


  val modelFile = "trained/acc_96.68-lr_0.01-batch_5-epoch_30-784x30x10.net"
  val model = Perceptron.load(modelFile, ActivationFunc.ReLU)

  override def classify(data: ImageData): ClassifyResult = {
    ClassifyResult((0 to 9) zip model.classify(data.input))
  }

  override def getExample: Future[NeuralNet] = Future{
    val params = PerceptronParams.load("trained/acc_96.92-lr_0.01-batch_5-epoch_30-784x30x10.net")
    NeuralNet(
      params.shape,
      ActivationFunc.LeakyReLU.name,
      withSoftMax = true,
      learningRate = 0.01,
      batchSize = 5,
      epochs = 30,
      params = Params.fromPerceptronParams(params)
    )
  }

}
