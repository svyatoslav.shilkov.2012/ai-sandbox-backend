package services

import models.{ClassifyResult, ImageData, NeuralNet}

import scala.concurrent.Future

trait NNService {

  def classify(data: ImageData): ClassifyResult

  def getExample: Future[NeuralNet]

}
